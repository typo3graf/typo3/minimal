# Minimal required TYPO3 package for customized projects

This package provides composer requirements to the minimal set of a customized project with TYPO3.


## Installation

`composer require t3graf/minimal`

This will install a TYPO3 instance and secure the complete system.

If it is called in a composer.json file, the following settings are required in the composer.json file:

* "extra":{
      "typo3/cms": {
        "root-dir": "private",
        "web-dir": "public"
    },

Additional TYPO3 extensions can subsequently be added with composer.

E.g.: `composer require typo3/cms-felogin`
